# Zfs Install\`N\`Emergency Repair tool

Shell (/bin/sh) dialog tool for Root-on-ZFS.

Linted with ShellCheck

Tested on ubuntu bionic.

### Working features:
* zpool with single plain vdev
* build zpool by /dev/disk/by-id devices
* zfs dataset hierarchy, with legacy fstab option for /var/log and /var/tmp
* EFI install
* align zpool at metaslab boundary
* make swap in useless tail and/or instead few trailing metaslabs (by decreasing zpool partition size)
* tmpfs for /tmp
* if swap choosed, zswap with z3fold configured, to tradeoff io usage to CPU while swapping and speed it up
* debootstrap as install method
* additional `payload` folder for configs, special binaries and all-over-you-want to copy to target system
* grub preconfiguration for better debugging while system boot up

### Known issues:
- none

### Used software
* lsb
* whiptail
* apt
* debootstrap
* gdisk
* zfs-initramfs
* mdadm
* mkswap
* lsblk

### Todo

+ mirror install
+ crypt install
+ more distros support
+ answer file support
